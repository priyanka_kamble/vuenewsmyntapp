import Vue from "vue";
import VueRouter from "vue-router";
import App from "./App.vue";
import { routes } from "./route/routes";
import VueResource from "vue-resource";
import store from "./store/store";
import { formatDate } from "./filter/date-filter";
Vue.use(VueRouter);
const router = new VueRouter({
  mode: "history",
  routes,
});
Vue.use(VueResource);
new Vue({
  el: "#app",
  router,
  store,
  formatDate,
  render: (h) => h(App),
});
