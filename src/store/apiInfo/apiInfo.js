import Vue from "vue";
import vuex from "vuex";
import VueResource from "vue-resource";
Vue.use(vuex);
Vue.use(VueResource);

export default {
  state: {
    isFilterDivShow: false,
    isFilterSearch: false,
    isLoaderShow: false,
    newsFilterCount: 0,
    newsListCount: 0,
    maxPerPage: 10,
    currentPageNews: 1,
    currentPageFilterNews: 1,
    filterCountry: "0",
    filterCategory: "0",
    keywords: "",
    newsListStore: [],
    newsFilterStore: [],
  },

  getters: {
    newsListStore: (state) => {
      return state.newsListStore;
    },
    newsListCount: (state) => {
      return state.newsListCount;
    },
    newsFilterStore: (state) => {
      return state.newsFilterStore;
    },
    newsFilterCount: (state) => {
      return state.newsFilterCount;
    },
    isFilterDivShow: (state) => {
      return state.isFilterDivShow;
    },
    filterCountry: (state) => {
      return state.filterCountry;
    },
    filterCategory: (state) => {
      return state.filterCategory;
    },
    keywords: (state) => {
      return state.keywords;
    },
    isFilterSearch: (state) => {
      return state.isFilterSearch;
    },
    currentPageNews: (state) => {
      return state.currentPageNews;
    },
    currentPageFilterNews: (state) => {
      return state.currentPageFilterNews;
    },
    isLoaderShow: (state) => {
      return state.isLoaderShow;
    },
  },

  mutations: {
    saveNewsList(state, newsList) {
      if (state.currentPageNews == 1) {
        state.newsListStore = newsList.articles;
      } else {
        newsList.articles.forEach((element) => {
          state.newsListStore.push(element);
        });
      }
      state.newsListCount = newsList.totalResults;
      state.isFilterDivShow = false;
    },
    saveFilterNewsList(state, filterNewsList) {
      if (state.currentPageFilterNews == 1) {
        state.newsFilterStore = filterNewsList.articles;
      } else {
        filterNewsList.articles.forEach((element) => {
          state.newsFilterStore.push(element);
        });
      }
      state.newsFilterCount = filterNewsList.totalResults;
      state.isFilterDivShow = false;
      if (state.newsFilterCount === 0) {
        state.isFilterDivShow = true;
        state.newsFilterStore = state.newsListStore;
        state.newsFilterCount = state.newsListCount;
      }
    },
    savefilterCategory(state, data) {
      state.filterCategory = data;
    },
    savefilterCountry(state, data) {
      state.filterCountry = data;
    },
    savefilterKeywords(state, data) {
      state.keywords = data;
    },
    saveCurrentPageNews(state, data) {
      state.currentPageNews = data;
    },
    saveCurrentPageFilterNews(state, data) {
      state.currentPageFilterNews = data;
    },
  },
  
  actions: {
    setfilterCategory: ({ commit }, order) => {
      commit("savefilterCategory", order);
    },
    setfilterCountry: ({ commit }, order) => {
      commit("savefilterCountry", order);
    },
    setfilterkeywords: ({ commit }, order) => {
      commit("savefilterKeywords", order);
    },
    setCurrentPageNews: ({ commit }, order) => {
      commit("saveCurrentPageNews", order);
    },
    setCurrentPageFilterNews: ({ commit }, order) => {
      commit("saveCurrentPageFilterNews", order);
    },
    setNewsListData({ commit, state }) {
      state.isFilterSearch = false;
      state.isLoaderShow = true;
      var payload = {
        maxPerPage: state.maxPerPage,
        currentPageNews: state.currentPageNews,
        keywordsFilter: state.keywords == "" ? "apple" : state.keywords,
      };
      fetch(
        "https://newsapi.org/v2/everything?q=" +
          payload.keywordsFilter +
          "&pageSize=" +
          payload.maxPerPage +
          "&page=" +
          payload.currentPageNews +
          "&apiKey=52558bd4559b49a8be19c436e85c750f"
      )
        .then((responce) => {
          return responce.json();
        })
        .then((data) => {
          commit("saveNewsList", data);
          state.isLoaderShow = false;
        });
    },
    setFilterNewsData({ commit, state }) {
      state.isFilterSearch = true;
      state.isLoaderShow = true;
      var payload = {
        maxPerPage: state.maxPerPage,
        currentPageFilterNews: state.currentPageFilterNews,
        filterCountry: state.filterCountry,
        filterCategory: state.filterCategory,
        keywords: state.keywords,
      };
      fetch(
        "https://newsapi.org/v2/top-headlines?country=" +
          payload.filterCountry +
          "&category=" +
          payload.filterCategory +
          "&pageSize=" +
          payload.maxPerPage +
          "&page=" +
          payload.currentPageFilterNews +
          "&q=" +
          payload.keywords +
          "&apiKey=52558bd4559b49a8be19c436e85c750f"
      )
        .then((responce) => {
          return responce.json();
        })
        .then((data) => {
          commit("saveFilterNewsList", data);
          state.isLoaderShow = false;
        });
    },
  },
};
