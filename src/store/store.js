import Vue from "vue";
import vuex from "vuex";
import bookmark from "./bookmark/bookmark";
import apiStore from "./apiInfo/apiInfo";
Vue.use(vuex);
export default new vuex.Store({
  modules: {
    bookmark,
    apiStore,
  },
});
