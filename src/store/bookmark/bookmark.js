export default {
  state: {
    bookMarkInfo: [],
  },
  mutations: {
    setBookmarkInfo(state, data) {
      state.bookMarkInfo = data;
    },
  },
  actions: {
    setBookmarkList: ({ commit }, order) => {
      commit("setBookmarkInfo", order);
    },
  },
  getters: {
    bookMarkInfo: (state) => {
      return state.bookMarkInfo;
    },
  },
};
