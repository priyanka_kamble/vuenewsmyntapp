import Home from "../components/Home/Home.vue";
import Newsdetails from "../components/News/Newsdetails.vue";
import Bookmark from "../components/Bookmark/Bookmark.vue";
export const routes = [
  { path: "/", component: Home, name: "Home" },
  { path: "/Newsdetails/:id", component: Newsdetails, name: "Newsdetails" },
  { path: "/Bookmark", component: Bookmark, name: "Bookmark" },
];
